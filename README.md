# kappi-react
The glue that makes kappi's state managment work with react components.

## Install
```
yarn add kappi-react
```
Peer dependencies:
```
react@^16.6.0
kappi@^0.2.3
```

## Usage
```tsx
// Class Component
class ChangingMessage extends WatcherComponent<{}, { msgText: string }> {
  createState () { return { msgText: 'Click Me' }; }
  handleClick = () => set(this.state, 'msgText', 'Hello World');
  static stateKeyGraph = parseKeyGraph('msgText');
  render () {
    return <div onClick={this.handleClick}>{this.state.msgText}</div>;
  }
}

// Props Function Component
const Message = createPropsWatcherComponent<{ msgText: string }>(
  'msgText',
  (props) => <div>{props.msgText}</div>
);

// State Component
class LCMessageState {
  constructor (
    public props: { msgText: string }
  ) {}
  @computed('props.msgText')
  get lowerCaseText () {
    return this.props.msgText.toLowerCase();
  }
  @computed('lowerCaseText')
  get renderNode () {
    return <div>{this.lowerCaseText}</div>;
  }
}

const LowerCaseMessage = createStateWatcherComponent(LCMessageState);
```
