import { WatcherComponent, createPropsWatcherComponent, createStateWatcherComponent } from './main';
import { shallow, mount } from 'enzyme';
import { spy, SinonSpy } from 'sinon';
import { expect } from 'chai';
import { createElement } from 'react';
import { set } from 'kappi';
import { watches, computed } from 'kappi/watcher';
import parseKeyGraph from 'kappi/key-graph/parse';

type Msg = { text: string };

interface MessageDisplayProps {
  msg: Msg;
}

class MessageDisplay extends WatcherComponent<MessageDisplayProps> {
  static propsKeyGraph = parseKeyGraph('msg.text');
  render () {
    return <div>{this.props.msg.text}</div>;
  }
}

describe('Component with renderWatch', () => {
  before(() => {
    spy(MessageDisplay.prototype, 'render');
  });
  after(() => {
    (MessageDisplay.prototype.render as SinonSpy).restore();
  });
  beforeEach(() => {
    (MessageDisplay.prototype.render as SinonSpy).resetHistory();
  });
  it('triggers rerender on set of watched', () => {
    const msg = { text: 'hello' };
    const wrapper = shallow(<MessageDisplay msg={msg} />);
    expect(wrapper.html()).to.equal('<div>hello</div>');
    set(msg, 'text', 'good bye');
    expect(wrapper.html()).to.equal('<div>good bye</div>');
    expect(
      (MessageDisplay.prototype.render as SinonSpy).callCount
    ).to.equal(2);
  });
  it('avoids rerender on shallow equal args change', () => {
    const msg = { text: 'hello' };
    const wrapper = shallow(<MessageDisplay msg={msg} />);
    expect(wrapper.html()).to.equal('<div>hello</div>');
    wrapper.setProps({ msg });
    expect(
      (MessageDisplay.prototype.render as SinonSpy).callCount
    ).to.equal(1);
  });
  it('triggers rerender on shallow unequal args change', () => {
    const msg = { text: 'hello' };
    const wrapper = shallow(<MessageDisplay msg={msg} />);
    expect(wrapper.html()).to.equal('<div>hello</div>');
    const newMsg = { text: 'good bye' };
    wrapper.setProps({ msg: newMsg });
    expect(wrapper.html()).to.equal('<div>good bye</div>');
    expect(
      (MessageDisplay.prototype.render as SinonSpy).callCount
    ).to.equal(2);
  });
});

const FCMessageDisplay = createPropsWatcherComponent<MessageDisplayProps>(
  'msg.text',
  ({ msg }) => <div>{msg.text}</div>
);

describe('functionComponent', () => {
  before(() => {
    spy(FCMessageDisplay.prototype, 'render');
  });
  after(() => {
    (FCMessageDisplay.prototype.render as SinonSpy).restore();
  });
  beforeEach(() => {
    (FCMessageDisplay.prototype.render as SinonSpy).resetHistory();
  });
  it('triggers rerender on set of watched', () => {
    const msg = { text: 'hello' };
    const wrapper = shallow(<FCMessageDisplay msg={msg} />);
    expect(wrapper.html()).to.equal('<div>hello</div>');
    set(msg, 'text', 'good bye');
    expect(wrapper.html()).to.equal('<div>good bye</div>');
    expect(
      (FCMessageDisplay.prototype.render as SinonSpy).callCount
    ).to.equal(2);
  });
  it('avoids rerender on shallow equal args change', () => {
    const msg = { text: 'hello' };
    const wrapper = shallow(<FCMessageDisplay msg={msg} />);
    expect(wrapper.html()).to.equal('<div>hello</div>');
    wrapper.setProps({ msg });
    expect(
      (FCMessageDisplay.prototype.render as SinonSpy).callCount
    ).to.equal(1);
  });
  it('triggers rerender on shallow unequal args change', () => {
    const msg = { text: 'hello' };
    const wrapper = shallow(<FCMessageDisplay msg={msg} />);
    expect(wrapper.html()).to.equal('<div>hello</div>');
    const newMsg = { text: 'good bye' };
    wrapper.setProps({ msg: newMsg });
    expect(wrapper.html()).to.equal('<div>good bye</div>');
    expect(
      (FCMessageDisplay.prototype.render as SinonSpy).callCount
    ).to.equal(2);
  });
});

interface VariableProps {
  foo?: string;
  roo?: number;
}

const FCVariableProp = createPropsWatcherComponent<VariableProps>(
  'foo,roo',
  ({ foo, roo }) => <div>foo: {foo} | roo: {roo}</div>
);

describe('functionComponent (variable args)', () => {
  before(() => {
    spy(FCVariableProp.prototype, 'render');
  });
  after(() => {
    (FCVariableProp.prototype.render as SinonSpy).restore();
  });
  beforeEach(() => {
    (FCVariableProp.prototype.render as SinonSpy).resetHistory();
  });
  it('avoids rerender on shallow equal prop change', () => {
    const wrapper = shallow(<FCVariableProp foo='bar'/>);
    expect(wrapper.html()).to.equal('<div>foo: bar | roo: </div>');
    wrapper.setProps({ foo: 'bar' });
    expect(
      (FCVariableProp.prototype.render as SinonSpy).callCount
    ).to.equal(1);
  });
  it('triggers rerender on watched props change', () => {
    const wrapper = shallow(<FCVariableProp foo='bar'/>);
    wrapper.setProps({ foo: 'bar', roo: 2 });
    expect(wrapper.html()).to.equal('<div>foo: bar | roo: 2</div>');
    expect(
      (FCVariableProp.prototype.render as SinonSpy).callCount
    ).to.equal(2);
  });
});

interface InnerProps {
  isOpen: boolean;
  thingColour: string;
}
interface DoorProps {
  innerProps: InnerProps;
}

const Door = createPropsWatcherComponent<DoorProps>(
  'innerProps.isOpen',
  ({ innerProps }) => {
    return <div>
      {innerProps.isOpen ? <Thing doorProps={innerProps} /> : 'closed'}
    </div>;
  }
);

interface ThingProps {
  doorProps: InnerProps;
}

class ThingState {
  props: ThingProps;
  constructor (props: ThingProps) {
    this.props = props;
  }
  @watches('props.doorProps.thingColour')
  updateDoorIsOpen () {
    const { doorProps } = this.props;
    set(doorProps, 'isOpen', doorProps.thingColour !== 'blue');
  }
}

class Thing extends WatcherComponent<ThingProps, ThingState> {
  createState (props: ThingProps) {
    return new ThingState(props);
  }
  static propsKeyGraph = parseKeyGraph('doorProps.{isOpen,thingColour}');
  render () {
    const { thingColour, isOpen } = this.props.doorProps;
    return `I am ${thingColour} and my door should be ${isOpen ? 'open' : 'closed'}`;
  }
}

describe('optimized rendering', () => {
  before(() => {
    spy(Thing.prototype, 'render');
    spy(Door.prototype, 'render');
  });
  after(() => {
    (Thing.prototype.render as SinonSpy).restore();
    (Door.prototype.render as SinonSpy).restore();
  });
  it('does optimize rendering', () => {
    const innerProps: InnerProps = { isOpen: true, thingColour: 'red' };
    const wrapper = mount(<Door innerProps={innerProps} />);
    expect(wrapper.html()).to.equal('<div>I am red and my door should be open</div>');
    expect((Thing.prototype.render as SinonSpy).callCount).to.equal(1, '1st check for thing');
    expect((Door.prototype.render as SinonSpy).callCount).to.equal(1, '1st check for door');
    set(innerProps, 'thingColour', 'blue');
    set(innerProps, 'thingColour', 'red');
    set(innerProps, 'thingColour', 'green');
    set(innerProps, 'thingColour', 'blue');
    expect((Thing.prototype.render as SinonSpy).callCount).to.equal(2, '2nd check for thing');
    expect((Door.prototype.render as SinonSpy).callCount).to.equal(2, '2nd check for door');
    expect(wrapper.html()).to.equal('<div>closed</div>');
    wrapper.unmount();
  });
});

class ComputerState {
  constructor (
    public props: { foo: string }
  ) {}
  @computed('props.foo')
  get bar () {
    return this.props.foo + 'bar';
  }
  @computed('bar')
  get renderNode () {
    return <div>{this.bar}</div>;
  }
}

const ComputedExample = createStateWatcherComponent(ComputerState);

describe('State Component', () => {
  before(() => {
    spy(ComputedExample.prototype, 'render');
  });
  after(() => {
    (ComputedExample.prototype.render as SinonSpy).restore();
  });
  beforeEach(() => {
    (ComputedExample.prototype.render as SinonSpy).resetHistory();
  });
  it('rerenders when props.foo changes', () => {
    const wrapper = shallow(<ComputedExample foo='fuz' />);
    expect(wrapper.html()).to.equal('<div>fuzbar</div>');
    wrapper.setProps({ foo: 'foo' });
    expect(wrapper.html()).to.equal('<div>foobar</div>');
  });
});
