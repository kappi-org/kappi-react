import { startWatcher, stopWatcher } from 'kappi/watcher';
import parseKeyGraph, { KeyGraph } from 'kappi/key-graph/parse';
import { Component, ReactNode } from 'react';
import { notifyChange } from 'kappi/internals';
import { watch, set } from 'kappi';

const internalPropsMap = new WeakMap<WatcherComponent<any, any>, any>();
const isInSCU = new WeakSet<WatcherComponent<any, any>>();
const shouldUpdate = new WeakSet<WatcherComponent<any, any>>();
const isInRender = new WeakSet<WatcherComponent<any, any>>();

const propsWatchTerminatorMap = new WeakMap<{}, () => void>();
const stateWatchTerminatorMap = new WeakMap<{}, () => void>();

type WithChildren<P> = P & { children?: ReactNode };

export class WatcherComponent <
  _P extends {} = {},
  S extends {} = {},
  P extends WithChildren<_P> = WithChildren<_P>,
> extends Component<_P, S> {
  readonly state!: S;
  createState? (props: P): S;
  constructor (props: P) {
    super(props);
    const internalProps = { ...(props as object) } as P;
    internalPropsMap.set(this, internalProps);
    if (this.createState) {
      this.state = this.createState(internalProps);
    }
  }
  static propsKeyGraph?: KeyGraph;
  static stateKeyGraph?: KeyGraph;
  componentDidMount () {
    const { propsKeyGraph, stateKeyGraph } = this.constructor as typeof WatcherComponent;

    const { state } = this;
    if (state) {
      startWatcher(state!);
      if (stateKeyGraph) {
        const terminator = watch(state!, stateKeyGraph, () => {
          if (isInSCU.has(this)) {
            shouldUpdate.add(this);
          }
          else if (!isInRender.has(this)) {
            this.forceUpdate();
          }
        });
        stateWatchTerminatorMap.set(this, terminator);
      }
    }

    if (propsKeyGraph) {
      const props = internalPropsMap.get(this)!;
      const terminator = watch(props, propsKeyGraph, () => {
        if (isInSCU.has(this)) {
          shouldUpdate.add(this);
        }
        else if (!isInRender.has(this)) {
          this.forceUpdate();
        }
      });
      propsWatchTerminatorMap.set(this, terminator);
    }
  }
  componentWillUnmount () {
    const propsWatchTerminator = propsWatchTerminatorMap.get(this);
    if (propsWatchTerminator) {
      propsWatchTerminator();
    }
    const stateWatchTerminator = stateWatchTerminatorMap.get(this);
    if (stateWatchTerminator) {
      stateWatchTerminator();
    }
    const { state } = this;
    if (state) {
      stopWatcher(state!);
    }
  }
  shouldComponentUpdate (nextProps: Readonly<P>, nextState: {}): boolean {
    shouldUpdate.delete(this);
    isInSCU.add(this);

    const internalProps = internalPropsMap.get(this)!;

    const { props } = this;
    const propKeys = new Set(Object.keys(props));
    const nextPropEntries = Object.entries(nextProps);

    for (const [key, value] of nextPropEntries as [string, any][]) {
      propKeys.delete(key);
      set(internalProps, key, value);

    }
    for (const key of propKeys) {
      delete internalProps[key];
      notifyChange(internalProps, key, undefined);
    }

    isInSCU.delete(this);
    return shouldUpdate.has(this);
  }
  setState () {
    throw new Error('Use kappi set instead');
  }
}

export function createPropsWatcherComponent <P extends {} = {}> (
  keyGraph: KeyGraph,
  renderer: (props: P, context?: any) => ReactNode
) {
  if (typeof keyGraph === 'string') {
    keyGraph = parseKeyGraph(keyGraph);
  }

  class FC extends WatcherComponent<P> {
    static propsKeyGraph = keyGraph;
    render () {
      return renderer(this.props, this.context);
    }
  }

  return FC as {
    new (props: P): WatcherComponent<P>;
    prototype: WatcherComponent<any>;
    propsKeyGraph: KeyGraph;
  };
}

interface State {
  renderNode: ReactNode;
}

interface StateClass <P extends {} = {}> {
  new (props: P): State;
}

const stateComponentKeyGraph = 'renderNode';

export function createStateWatcherComponent <P extends {} = {}> (StateClass: StateClass<P>) {
  class StateComponent extends WatcherComponent<P, State> {
    createState (props: WithChildren<P>) {
      return new StateClass(props);
    }
    static stateKeyGraph = stateComponentKeyGraph;
    render () {
      return this.state.renderNode;
    }
  }

  return StateComponent as {
    new (props: P): WatcherComponent<P, State>;
    prototype: WatcherComponent<any, any>;
    stateKeyGraph: KeyGraph;
  };
}

export const createSWC = createStateWatcherComponent;
export const createPWC = createPropsWatcherComponent;


