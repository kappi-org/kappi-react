import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import { JSDOM, DOMWindow } from 'jsdom';

declare var global: NodeJS.Global & {
  window: DOMWindow;
  document: Document;
};

const dom = new JSDOM('<html><body></body></html>');
global.window = dom.window;
global.document = dom.window.document;

Enzyme.configure({ adapter: new Adapter() });